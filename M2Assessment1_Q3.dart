void main() {
  var ap = winApp();
  ap.appName = "Ambani Africa App";
  ap.category = "Best South African Solution";
  ap.developer = "Mukundi Lambani";
  ap.wonYearAward = 2021;

  ap.winningApp();
}

class winApp {
  String? appName;
  String? category;
  String? developer;
  int? wonYearAward;

  void winningApp() {
    print("$appName".toUpperCase() + ", $category, $developer, $wonYearAward");
  }
}
